#!/bin/ash

opkg update

# Install all always needed packages
opkg install luci luci-app-attendedsysupgrade luci-proto-wireguard vim ca-bundle 6in4 iperf3-ssl

# Upgrade all installed Packages
opkg list-upgradable | cut -f 1 -d ' ' | xargs opkg upgrade

#Add Cronjobs
