#!/bin/bash

### Run this script on a linux computer, to create the Ubiquiti OpenWRT boot drive (USB). File "create_boot-usb_fdisk-cmds" is (maybe) needed in the same directory!

rm -rf tmp-owrt-create-bootusb


read -p "OpenWRT 'https://downloads.openwrt.org/releases/...squashfs-sysupgrade.tar' download link: " sysupgradelnk
read -p "Path to USB Stick: " usbdev


mkdir -p tmp-owrt-create-bootusb/mnt-kernel-part
cd tmp-owrt-create-bootusb
wget -O sysupgrade.tar $sysupgradelnk
tar -xf sysupgrade.tar
rm -f sysupgrade.tar
mv sysupgrade* sysupgrade

mv sysupgrade/root root
mv sysupgrade/kernel vmlinux.64
md5sum vmlinux.64 | cut -d' ' -f 1 > vmlinux.64.md5
rm -rf sysupgrade


read -p "Do you want to proceed partitioning usb dev $usbdev? (yes/no) " yn

case $yn in
	yes ) echo starting..........;;
	* ) echo aborted! exiting...;
	    exit;;
esac


#sudo fdisk $usbdev < ../create_boot-usb_fdisk-cmds
sudo sfdisk --label dos $usbdev
sleep 5
echo -e ',500M,c\n,+,\n' | sudo sfdisk $usbdev
echo "------ Partitions created ------"
sleep 5

sudo mount ${usbdev}1 mnt-kernel-part
echo "------ kernel partition mounted ------"


sudo cp vmlinux.64* mnt-kernel-part/
sudo dd if=root of=${usbdev}2 bs=4096


sudo umount mnt-kernel-part
sync
rm -rf tmp-owrt-create-bootusb


echo
echo "-----------------------------------------------------------------------------------------------------------"
echo "Creation of Ubiquiti Boot USB Drive done! Please remove the USB Drive now and place it back on your Router!"
echo "-----------------------------------------------------------------------------------------------------------"
