#!/bin/bash

docker rm $(docker ps -aq); docker rmi $(docker images -q); docker volume prune -f; docker system prune -f