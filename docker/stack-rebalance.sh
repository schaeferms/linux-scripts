#!/bin/bash

(set -e && EXCLUDE_LIST="(prune|NAME)" && for service in $(docker service ls | egrep -v $EXCLUDE_LIST | awk '{print $2}'); do docker service update --force $service; done)