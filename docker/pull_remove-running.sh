curl -fsSL https://gitlab.com/schaeferms/linux-scripts/-/raw/main/docker/pull-latest.sh | bash

docker rm -f $(docker ps -q)

echo ""
echo "################################"
echo ">>> Latest images pulled and all running container where killed! <<<"