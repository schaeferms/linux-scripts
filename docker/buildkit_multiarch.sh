#!/bin/bash

## Use it like build-push_docker-universal.sh [FOLDER/IMAGE/GitRepo-Name]
## Example: ./build-push_docker-universal.sh "docker-vlmcsd"

DOCKER=$(which docker)
IMAGE_NAME=$1
CONTEXT_FOLDER=${IMAGE_NAME}
DOCKERFILE_NAME="Dockerfile" 
TAG="latest"
HTTP_PROXY=

source ../GLOBAL_VARS 2> /dev/null
source GLOBAL_VARS 2> /dev/null
source ${IMAGE_NAME}/vars 2> /dev/null
source vars 2> /dev/null

BUILDER=${IMAGE_NAME}"-builder"
BUILD_DATE=$(date -u '+%Y-%m-%dT%H:%M:%S%z')
TAG_TIMESTAMP=$(date -u '+%Y%m%dT%H%M%S')
BUILD_VERSION="build_"${TAG_TIMESTAMP}
BUILD_OPTS=$2



echo "Building and pushing: "${IMAGE_NAME}


BUILDER_EXISTS=$($DOCKER buildx ls| grep ${BUILDER})
# $DOCKER run --rm --privileged multiarch/qemu-user-static --reset -p yes > /dev/null
if [ ! -n "$BUILDER_EXISTS" ]; then
  $DOCKER buildx create --name ${BUILDER} --use --driver remote tcp://localhost:1234
else
  $DOCKER buildx use ${BUILDER}
fi

$DOCKER buildx build --builder ${BUILDER} \
                     --platform ${BUILD_PLATFORMS} \
                     --build-arg BUILD_DATE=${BUILD_DATE} \
                     --build-arg VERSION=${BUILD_VERSION} \
                     --build-arg HTTP_PROXY=${HTTP_PROXY} \
                     --tag ${CONTAINER_REGISTRY}${REGISTRY_USER}/${IMAGE_NAME}:${TAG} \
                     --tag ${CONTAINER_REGISTRY}${REGISTRY_USER}/${IMAGE_NAME}:${TAG_TIMESTAMP} \
                     --push --pull $BUILD_OPTS -f ${CONTEXT_FOLDER}/${DOCKERFILE_NAME} ${CONTEXT_FOLDER}


# echo "Cleaning up the builder for "${IMAGE_NAME}
# $DOCKER rm -f $($DOCKER ps --filter "name=buildx_buildkit_$BUILDER" --format "{{.ID}}") > /dev/null


echo "------------------------>>>>>>>>>> FINISHED <<<<<<<<<<------------------------"
echo "------------------------>>>>>>>>>> "${IMAGE_NAME}" <<<<<<<<<<------------------------"
echo "------------------------>>>>>>>>>> FINISHED <<<<<<<<<<------------------------"
