#!/bin/bash

do-release-upgrade --proposed --allow-third-party -f DistUpgradeViewNonInteractive

curl -fsSL https://gitlab.com/schaeferms/linux-scripts/-/raw/main/debian/apt/upgrade-all.sh | bash