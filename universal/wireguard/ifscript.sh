#!/bin/bash

action=$1
interface=$2
int_interface=$3

[[ "$action" = "up" ]] && action_arg="A" || action_arg="D"

log () {
  [[ "$action" = "up" ]] && local cmd_action="Enabling" || local cmd_action="Disabling"
  echo "[ifscript.sh] $cmd_action $@"
}

log "sysctl ip forward rule for wireguard"

[[ "$action" = "up" ]] && sysctl -w net.ipv4.ip_forward=1
[[ "$action" = "up" ]] && sysctl -w net.ipv6.conf.all.forwarding=1



###############################

#log "iptables rules for ${interface}"

# Allow peers to connect to the wireguard interface
#iptables -$action_arg FORWARD -i $interface -j ACCEPT
#iptables -$action_arg FORWARD -o $interface -j ACCEPT

#iptables -t nat -$action_arg POSTROUTING -o $int_interface -j MASQUERADE

#iptables -A INPUT -p udp -m udp --dport 51820 -j ACCEPT

# IPv6
#ip6tables -$action_arg FORWARD -i $interface -j ACCEPT
#ip6tables -$action_arg FORWARD -o $interface -j ACCEPT

#ip6tables -t nat -$action_arg POSTROUTING -o $int_interface -j MASQUERADE

#ip6tables -A INPUT -p udp -m udp --dport 51820 -j ACCEPT
