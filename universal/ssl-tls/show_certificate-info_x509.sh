#!/bin/bash

CERT_FILE=$1

echo "SSL/TLS Show important infos for a certificate file (Script Paramenter 1 = Path and Filename to x509 certificate file)"
echo
openssl x509 -nocert -subject -alias -email -in $CERT_FILE
echo
openssl x509 -nocert -dates -in $CERT_FILE
echo
echo
openssl x509 -nocert -issuer -in $CERT_FILE
echo
openssl x509 -nocert -purpose -in $CERT_FILE
echo
echo
openssl x509 -nocert -modulus -in $CERT_FILE
echo
echo "Filename: $CERT_FILE"

