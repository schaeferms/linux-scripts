#!/bin/bash

PRIVKEY_FILE=$1

echo "SSL/TLS Private Key Integrity check for a keyfile (Script Paramenter 1 = Path and Filename to keyfile)"
echo
echo "### Test 1:"
openssl rsa -in $PRIVKEY_FILE -check -noout

echo
echo "###### All tests finished ######"
echo
echo "Private Key Modulus:"
openssl rsa -noout -modulus -in $PRIVKEY_FILE

