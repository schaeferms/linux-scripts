#!/bin/bash

curl -fsSL https://gitlab.com/schaeferms/linux-scripts/-/raw/main/debian/apt/upgrade-all.sh | bash
apt -y install software-properties-common
apt-add-repository -y --update ppa:ansible/ansible

apt -y update
apt -y install ansible
