#!/bin/bash
DEBIAN_FRONTEND=noninteractive
APT_PARAMS='-y --allow-change-held-packages'
APT_PARAMS_UPGRADE='-o Dpkg::Options::="--force-confold" -y --allow-downgrades --allow-change-held-packages'

SUDO_CMD=''
if [[ $EUID -ne 0 ]]; then
# not root - Check if sudo exists
  SUDO_CMD='sudo'
  if ! [ -x "$(command -v $SUDO_CMD)" ]; then
    echo "You must be a root user" 2>&1
    exit 1
  fi
fi

export DEBIAN_FRONTEND=noninteractive # Needed or not???

$SUDO_CMD dpkg --configure -a
$SUDO_CMD apt $APT_PARAMS --fix-broken install
$SUDO_CMD apt $APT_PARAMS update -y
$SUDO_CMD apt $APT_PARAMS_UPGRADE upgrade;
$SUDO_CMD apt $APT_PARAMS_UPGRADE full-upgrade
$SUDO_CMD apt $APT_PARAMS --purge autoremove
