#!/bin/bash

DIR=$1

find $DIR -type d -exec chmod 775 {} \;

find $DIR -type f -exec chmod 664 {} \;

