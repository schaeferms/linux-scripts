#!/bin/bash

DIR=$1

find $DIR -exec chmod -t {} \;

echo "Done: find $DIR -exec chmod -t {} \\;"
