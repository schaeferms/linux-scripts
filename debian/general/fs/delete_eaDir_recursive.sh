#!/bin/bash

echo "Files & Folders before cleaning: "
find . | wc -l

echo 'Removing Folders by name: "@eaDir"....'
find . -type d -name "@eaDir" -exec rm -rf \;

echo "Files & Folders now: "
find . | wc -l

